import React from 'react';
import './App.css';
import CompanyList from './components/CompanyList'
import CompanyForm from './components/CompanyForm'

interface Company {
  name?: string,
  companyCode?: string,
  email?: string,
  homepage?: string,
  phoneNumber?: string,
  streetAddress?: string
}
interface AppState {
  rows: Company[]
}

class App extends React.Component<{}, AppState> {

  constructor(props: any) {
    super(props)
    this.state = { rows: [] }
    this.handleNewCompany = this.handleNewCompany.bind(this);
  }

  componentDidMount() {
    fetch("http://localhost:8080/api/companies")
      .then(async (data) => this.setState({
        rows: await data.json()
      }))
  }

  handleNewCompany(company: Company) {
    console.log(this.state.rows)
    var newRows = this.state.rows.concat(company)
    console.log(newRows)
    this.setState({ rows: newRows })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Yritystieto</h1>
        </header>
        <section className="App-content">
          <CompanyList rows={this.state.rows}></CompanyList>
          <CompanyForm onCreateNewCompany={this.handleNewCompany}></CompanyForm>
        </section>
      </div>
    );
  }
}

export default App;
