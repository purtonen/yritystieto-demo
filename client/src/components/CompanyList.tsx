import * as React from 'react';

import Box from '@mui/material/Box';
import { DataGrid, GridColDef } from '@mui/x-data-grid';

const columns: GridColDef[] = [
    {
        field: 'companyCode',
        headerName: 'Y-tunnus',
        flex: 1,
        editable: false,
    },
    {
        field: 'name',
        headerName: 'Nimi',
        flex: 1,
        editable: false,
    },
    {
        field: 'email',
        headerName: 'Sähköposti',
        flex: 1,
        editable: false,
    },
    {
        field: 'homepage',
        headerName: 'Kotisivu',
        flex: 1,
        editable: false,
    },
    {
        field: 'phoneNumber',
        headerName: 'Puhelinnumero',
        flex: 1,
        editable: false,
    },
    {
        field: 'streetAddress',
        headerName: 'Osoite',
        flex: 2,
        editable: false,
    },
];


class CompanyList extends React.Component<{rows: object[]}, {}> {

    render(): React.ReactNode {
        return (
            <section className='section-component'>
            <Box sx={{ height: 400, width: '100%' }}>
                <DataGrid
                    rows={this.props.rows}
                    columns={columns}
                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    checkboxSelection
                    disableSelectionOnClick
                />
            </Box>
            </section>
        );
    }
}

export default CompanyList