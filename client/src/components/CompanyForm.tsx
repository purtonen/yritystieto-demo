import { Box, Button, TextField } from '@mui/material';
import * as React from 'react';


class CompanyForm extends React.Component<{
    onCreateNewCompany: (company: {
        name?: string,
        companyCode?: string,
        email?: string,
        homepage?: string,
        phoneNumber?: string,
        streetAddress?: string
    }) => void
}, {
    name?: string,
    companyCode?: string,
    email?: string,
    homepage?: string
}> {

    constructor(props: any) {
        super(props)
        this.state = {
            name: '',
            companyCode: '',
            email: '',
            homepage: ''
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.addNewCompany = this.addNewCompany.bind(this);
    }

    addNewCompany(): void {
        fetch("http://localhost:8080/api/companies", {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(this.state)
        })
            .then(async (data) => {
                this.props.onCreateNewCompany(await data.json())
                this.setState({
                    name: '',
                    companyCode: '',
                    email: '',
                    homepage: ''
                })
            })
    }

    handleInputChange(event: any) {
        const target = event.target;
        const value: string = target.type === 'checkbox' ? target.checked : target.value;
        const name: string = target.name;

        this.setState({
            [name]: value
        });
    }

    render(): React.ReactNode {
        return (
            <section className="section-component">
                <h3>Lisää uusi yritys</h3>
                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '25ch' },
                    }}
                    noValidate
                    autoComplete="off">
                    <TextField name="name" value={this.state.name} onChange={this.handleInputChange} variant="standard" label="Nimi" required />
                    <TextField name="companyCode" value={this.state.companyCode} onChange={this.handleInputChange} variant="standard" label="Y-tunnus" />
                    <TextField name="email" value={this.state.email} onChange={this.handleInputChange} variant="standard" label="Sähköposti" />
                    <TextField name="homepage" value={this.state.homepage} onChange={this.handleInputChange} variant="standard" label="Kotisivu" />
                    <Button onClick={this.addNewCompany} variant="contained">Lisää</Button>
                </Box>
            </section>
        );
    }
}

export default CompanyForm