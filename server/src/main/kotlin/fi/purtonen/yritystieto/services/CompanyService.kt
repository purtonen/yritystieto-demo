package fi.purtonen.yritystieto.services

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import fi.purtonen.yritystieto.data.CompanyEntity
import fi.purtonen.yritystieto.data.CompanyRepository
import fi.purtonen.yritystieto.models.Company
import fi.purtonen.yritystieto.models.CompanyEnrichmentData
import fi.purtonen.yritystieto.models.CompanyRequest
import fi.purtonen.yritystieto.models.CompanyResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.net.URI
import java.util.*
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

@Service
class CompanyService (var repository: CompanyRepository) {

    fun EnrichCompanyData(companyRequest: CompanyRequest) : Company {
        if(companyRequest.companyCode == null || companyRequest.companyCode == "")
            return companyRequest.toCompany()

        val client = HttpClient.newBuilder().build();
        val request = HttpRequest.newBuilder()
            .uri(URI.create("https://www.kauppalehti.fi/company-api/basic-info/${companyRequest.companyCode}"))
            .build();

        val response = client.send(request, HttpResponse.BodyHandlers.ofString());
        val mapper = jacksonObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false) // Ignore unknown fields
        val companyEnrichmentData: CompanyEnrichmentData = mapper.readValue(response.body().toString())

        return companyRequest.toEnrichedCompany(companyEnrichmentData)
    }

    fun GetAllCompanies() : List<CompanyResponse> {
        return repository.findAll().map{ it.toCompanyResponse() }
    }

    fun CreateCompany(company: Company) : CompanyResponse {
        return repository.save(company.toCompanyEntity())
            .toCompanyResponse()
    }

    fun CreateCompany(companyRequest: CompanyRequest) : CompanyResponse {
        return repository.save(companyRequest.toCompany().toCompanyEntity())
            .toCompanyResponse()
    }

    fun DeleteCompany(id: UUID){
        repository.deleteById(id)
    }
}