package fi.purtonen.yritystieto.services

import fi.purtonen.yritystieto.data.CompanyEntity
import fi.purtonen.yritystieto.models.Company
import fi.purtonen.yritystieto.models.CompanyEnrichmentData
import fi.purtonen.yritystieto.models.CompanyRequest
import fi.purtonen.yritystieto.models.CompanyResponse
import java.util.*


fun CompanyEntity.toCompanyResponse() = CompanyResponse(
    id,
    name,
    email,
    homepage,
    companyCode,
    streetAddress,
    phoneNumber
)

fun Company.toCompanyEntity() = CompanyEntity(
    UUID(0,0),
    name,
    email,
    homepage,
    companyCode,
    streetAddress,
    phoneNumber
)

fun CompanyRequest.toCompany() = Company(
    name,
    email,
    homepage,
    companyCode,
    null,
    null
)

fun CompanyRequest.toEnrichedCompany(companyEnrichmentData: CompanyEnrichmentData) = Company(
    name,
    email,
    homepage,
    companyCode,
    "${companyEnrichmentData.streetAddress.street.value}, ${companyEnrichmentData.streetAddress.city.value}, ${companyEnrichmentData.streetAddress.postalCode.value}",
    companyEnrichmentData.phone.value?.replaceRange(0, 1, "+358")
)