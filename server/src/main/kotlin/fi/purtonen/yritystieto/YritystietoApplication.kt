package fi.purtonen.yritystieto

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = ["fi.purtonen.yritystieto.api","fi.purtonen.yritystieto.data", "fi.purtonen.yritystieto.services"])
class YritystietoApplication

fun main(args: Array<String>) {
	runApplication<YritystietoApplication>(*args)
}
