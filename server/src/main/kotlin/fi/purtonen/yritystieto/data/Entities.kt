package fi.purtonen.yritystieto.data

import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "company")
data class CompanyEntity(
    @Id
    @Type(type="pg-uuid")
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: UUID,
    @Column(nullable = false)
    val name: String,
    @Column
    val email: String?,
    @Column
    val homepage: String?,
    @Column
    val companyCode: String?,
    @Column
    val streetAddress: String?,
    @Column
    val phoneNumber: String?
)