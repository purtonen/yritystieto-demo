package fi.purtonen.yritystieto.data

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*
import javax.persistence.Table

@Table(name = "company")
interface CompanyRepository : JpaRepository<CompanyEntity, UUID> {
    //fun findAllCompanies(): List<CompanyData>
}