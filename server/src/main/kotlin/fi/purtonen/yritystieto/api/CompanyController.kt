package fi.purtonen.yritystieto.api

import fi.purtonen.yritystieto.models.Company
import fi.purtonen.yritystieto.models.CompanyRequest
import fi.purtonen.yritystieto.models.CompanyResponse
import fi.purtonen.yritystieto.services.CompanyService
import fi.purtonen.yritystieto.services.toCompany
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/companies")
@CrossOrigin(origins = ["*"])
class CompanyController @Autowired constructor(private val companyService: CompanyService) {
    @GetMapping
    fun GetCompanies(): List<CompanyResponse> {
        return companyService.GetAllCompanies()
    }

    @PostMapping
    fun CreateCompany(
        @RequestBody
        companyRequest: CompanyRequest
    ): ResponseEntity<CompanyResponse> {
        if(!companyRequest.validate())
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        val company: Company = try{
            companyService.EnrichCompanyData(companyRequest)
        } catch (e: Exception) {
            companyRequest.toCompany()
        }
        return ResponseEntity(companyService.CreateCompany(company), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun DeleteCompany(@PathVariable id: UUID) {
        try{
            companyService.DeleteCompany(id)
        } catch (e: EmptyResultDataAccessException){

        }
    }

    fun CompanyRequest.validate() : Boolean{
        if(this.name == null || this.name == "")
            return false

        return true
    }
}


