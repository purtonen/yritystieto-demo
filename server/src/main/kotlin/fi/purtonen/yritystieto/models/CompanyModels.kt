package fi.purtonen.yritystieto.models

import java.util.*

data class Company (
    val name: String,
    val email: String?,
    val homepage: String?,
    val companyCode: String?,
    val streetAddress: String?,
    val phoneNumber: String?
)

data class CompanyResponse(
    val id: UUID,
    val name: String,
    val email: String?,
    val homepage: String?,
    val companyCode: String?,
    val streetAddress: String?,
    val phoneNumber: String?
)

data class CompanyRequest(
    val name: String,
    val email: String?,
    val homepage: String?,
    val companyCode: String?
)

data class CompanyEnrichmentData(
    val phone: Phone,
    val streetAddress: StreetAddress
){
    data class Phone(
        val value: String?
    )
    data class StreetAddress(
        val street: Street,
        val city: City,
        val postalCode: PostalCode,
    ){
        data class Street(
            val value: String?
        )
        data class City(
            val value: String?
        )
        data class PostalCode(
            val value: String?
        )
    }
}