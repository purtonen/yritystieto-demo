package fi.purtonen.yritystieto

import fi.purtonen.yritystieto.data.CompanyEntity
import fi.purtonen.yritystieto.data.CompanyRepository
import fi.purtonen.yritystieto.models.Company
import fi.purtonen.yritystieto.models.CompanyEnrichmentData
import fi.purtonen.yritystieto.models.CompanyRequest
import fi.purtonen.yritystieto.models.CompanyResponse
import fi.purtonen.yritystieto.services.toCompany
import fi.purtonen.yritystieto.services.toCompanyEntity
import fi.purtonen.yritystieto.services.toCompanyResponse
import fi.purtonen.yritystieto.services.toEnrichedCompany
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.util.AssertionErrors.assertEquals
import org.springframework.test.util.AssertionErrors.assertNotEquals
import java.util.*

@SpringBootTest
class YritystietoApplicationTests {

    val equalCompany = Company(
        "Name",
        "email",
        "homepage",
        "companyCode",
        "streetAddress",
        "phoneNumber"
    )
    val equalCompanyFromRequest = Company(
        "Name",
        "email",
        "homepage",
        "companyCode",
        null,
        null
    )
    val equalCompanyRequest = CompanyRequest(
        "Name",
        "email",
        "homepage",
        "companyCode"
    )
    val equalCompanyResponse = CompanyResponse(
        UUID(0, 0),
        "Name",
        "email",
        "homepage",
        "companyCode",
        "streetAddress",
        "phoneNumber"
    )
    val equalCompanyEntity = CompanyEntity(
        UUID(0, 0),
        "Name",
        "email",
        "homepage",
        "companyCode",
        "streetAddress",
        "phoneNumber"
    )
    val enrichmentData = CompanyEnrichmentData(
        CompanyEnrichmentData.Phone("0enrichedPhone"),
        CompanyEnrichmentData.StreetAddress(
            CompanyEnrichmentData.StreetAddress.Street("enrichedStreet"),
            CompanyEnrichmentData.StreetAddress.City("enrichedCity"),
            CompanyEnrichmentData.StreetAddress.PostalCode("enrichedPostCode")
        )
    )
    val equalEnrichedCompany = Company(
        "Name",
        "email",
        "homepage",
        "companyCode",
        "enrichedStreet, enrichedCity, enrichedPostCode",
        "+358enrichedPhone"
    )
    val unEqualCompany = Company(
        "notName",
        "email",
        "homepage",
        "companyCode",
        "streetAddress",
        "phoneNumber"
    )
    val unEqualCompanyResponse = CompanyResponse(
        UUID(0, 0),
        "notName",
        "email",
        "homepage",
        "companyCode",
        "streetAddress",
        "phoneNumber"
    )
    val unEqualCompanyEntity = CompanyEntity(
        UUID(0, 0),
        "notName",
        "email",
        "homepage",
        "companyCode",
        "streetAddress",
        "phoneNumber"
    )

    @Test
    fun `test extension function companyEntity toCompanyResponse`() {
        assertEquals("", equalCompanyResponse, equalCompanyEntity.toCompanyResponse())
        assertNotEquals("", unEqualCompanyResponse, equalCompanyEntity.toCompanyResponse())
    }

    @Test
    fun `test extension function company toCompanyEntity`() {
        assertEquals("", equalCompanyEntity, equalCompany.toCompanyEntity())
        assertNotEquals("", unEqualCompanyEntity, equalCompany.toCompanyEntity())
    }

    @Test
    fun `test extension function companyRequest toCompany`() {
        assertEquals("", equalCompanyFromRequest, equalCompanyRequest.toCompany())
        assertNotEquals("", unEqualCompany, equalCompanyRequest.toCompany())
    }

    @Test
    fun `test extension function companyRequest toEnrichedCompany`() {
        assertEquals("", equalEnrichedCompany, equalCompanyRequest.toEnrichedCompany(enrichmentData))
        assertNotEquals("", equalCompany, equalCompanyRequest.toEnrichedCompany(enrichmentData))
    }

}
